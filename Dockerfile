FROM node:7
WORKDIR /buildpatch

COPY package.json /buildpatch
RUN npm install
COPY . /buildpatch