import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// import { Observable } from "rxjs";
// used to create fake backend
import { fakeBackendProvider } from './shared/_helpers';

import { appRoutingModule } from './app.routing';
import { JwtInterceptor, ErrorInterceptor } from './shared/_helpers';
import { AppComponent } from './app.component';
import { HomeComponent } from './modules/home';
import { LoginComponent, AdminLoginComponent } from './modules/login';
import { RegisterComponent } from './modules/register';
import { HeaderComponent } from './core/header';
import { ICAdminModule } from '@/modules/ic/ic-admin/ic-admin.module';
import { AlertComponent } from './shared/_components';
import { LanguageSelectorComponent } from './core/language-selector';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { TranslateService } from '@ngx-translate/core';
import { InternationalizationService } from './shared/_services';
// import { LayoutComponent } from '@/core/layout/layout.component';
import { SidenavListComponent } from '@/core/sidenav-list/sidenav-list.component';
import {
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,

    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatTabsModule
} from '@angular/material';
import {
    NbChatModule,
    NbDatepickerModule,
    NbDialogModule,
    NbMenuModule,
    NbSidebarModule,
    NbToastrModule,
    NbWindowModule,
    NbSelectModule,
    NbThemeModule,
    NbLayoutModule
} from '@nebular/theme';
// import { NbThemeModule, NbLayoutModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';

// import {
//     OneColumnLayoutComponent,
//     ThreeColumnsLayoutComponent,
//     TwoColumnsLayoutComponent,
// } from './layouts';
// import { ThemeModule } from '@/core/@theme/theme.module';
import { FlexLayoutModule } from '@angular/flex-layout';

// import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
// import { makeStateKey, StateKey, TransferState } from '@angular/platform-browser';
import 'hammerjs';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'; // For tooltip
import { MatMenuModule } from '@angular/material/menu'; // For Menu icon
// export function HttpLoaderFactory(httpClient: HttpClient) {
//     return new TranslateHttpLoader(httpClient, './assets/i18n/', '.json');
// }
// import { DEFAULT_THEME } from '@/core/header/themes/theme.default';
// import { COSMIC_THEME } from '@/core/header/themes/theme.cosmic';
// import { CORPORATE_THEME } from '@/core/header/themes/theme.corporate';
// import { DARK_THEME } from '@/core/header/themes/theme.dark';
@NgModule({

    imports: [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatRippleModule,
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        appRoutingModule,
        MatSelectModule,
        BrowserAnimationsModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatRadioModule,
        FormsModule,
        ICAdminModule,
        TranslateModule.forRoot(),
        // TODO: what is forRoot in here without any input data
        MatSidenavModule,
        MatToolbarModule,
        MatIconModule,
        MatListModule,
        MatTabsModule,
        FlexLayoutModule,
        NgbModule,
        MatMenuModule,
        // NbThemeModule,
        // NbSelectModule,
        // ThemeModule.forRoot(),

        // NbSidebarModule.forRoot(),
        // NbMenuModule.forRoot(),
        // NbDatepickerModule.forRoot(),
        // NbDialogModule.forRoot(),
        // NbWindowModule.forRoot(),
        // NbToastrModule.forRoot(),
        // NbChatModule.forRoot({
        //     messageGoogleMapKey: 'AIzaSyA_wNuCzia92MAmdLRzmqitRGvCF7wCZPY',
        // }),
        // NbSelectModule,
        // NbThemeModule.forRoot({ name: 'default' }),
        // NbLayoutModule,
        // NbEvaIconsModule

    ],
    exports: [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatRippleModule,
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        MatSelectModule,
        BrowserAnimationsModule,
        MatDatepickerModule,
        ICAdminModule,
        MatSidenavModule,
        MatToolbarModule,
        MatIconModule,
        MatListModule,
        MatTabsModule
        // MatMenuModule
        // Observable
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        LoginComponent,
        AdminLoginComponent,
        RegisterComponent,
        AlertComponent,
        HeaderComponent,
        LanguageSelectorComponent,
        SidenavListComponent,
        // LayoutComponent
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        InternationalizationService,
        // provider used to create fake backend
        fakeBackendProvider
    ],
    bootstrap: [AppComponent]
})
export class AppModule { };