import { Component, OnInit } from '@angular/core';
// import { Store } from '@ngrx/store';
import { Language } from '../../shared/_models/language.model';
import { TranslateService } from '@ngx-translate/core';
import { InternationalizationService } from '../../shared/_services'
@Component({
  selector: 'app-language-selector',
  templateUrl: './language-selector.component.html'
})
export class LanguageSelectorComponent implements OnInit {
  constructor(
    //private store: Store<fromI18n.State>, 
    private inznservice: InternationalizationService,
    public translate: TranslateService
  ) {
    translate.addLangs(['en', 'vn']);
    translate.setDefaultLang('en');

    const browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/en|vn/) ? browserLang : 'en');
  }

  ngOnInit() { }

  setLanguage(language: Language) {
    // debugger;
    this.inznservice.language.next(language);
    // this.store.dispatch(LanguageActions.set({ language }));
  }
}
