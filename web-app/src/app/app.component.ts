import { Router } from '@angular/router';

import { UserService } from './shared/_services';
import { User } from './shared/_models';
import { Component, OnInit, PLATFORM_ID, APP_ID, Inject } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Observable, Subject, BehaviorSubject, of, throwError } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import './shared/_content/app.less';
import { HeaderComponent } from './core/header';
// import { select, Store } from '@ngrx/store';
// import { TranslateService } from '@ngx-translate/core';
import { InternationalizationService } from './shared/_services'; //this is the service

@Component({ selector: 'app', templateUrl: 'app.component.html', styleUrls: ['./app.component.scss'] })
export class AppComponent {
    currentUser: User;
    // translate: TranslateService;
    constructor(
        // private inznservice: InternationalizationService,
        private router: Router,
        private userService: UserService
    ) {
        this.userService.currentUser.subscribe(x => this.currentUser = x);
    }

    // logout() {
    //     this.userService.logout();
    //     this.router.navigate(['/login']);
    // }
    // useLanguage(lan: string) {
    //     this.inznservice.language.next(lan);
    // }
}