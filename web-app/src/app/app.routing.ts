import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './modules/home';
import { LoginComponent , AdminLoginComponent} from './modules/login';
import { RegisterComponent } from './modules/register';
import { AuthGuard } from './shared/_helpers';
// import { ICAdmin } from './modules/ic/ic-admin';

const routes: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'ic/admin/login', component: AdminLoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'ic/admin', loadChildren: () => import('@/modules/ic/ic-admin/ic-admin.module').then(m => m.ICAdminModule) },
    // { path: 'register', component: RegisterComponent },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const appRoutingModule = RouterModule.forRoot(routes);