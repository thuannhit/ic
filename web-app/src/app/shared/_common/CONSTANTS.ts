export const API_CONSTANT = {
    ADMIN_LOGIN: "admin/user/login",
    PUBLISH_IC: "admin/ic/publish_ic",
    GET_PUBLISHED_ICS_LIST: "admin/ic/get_published_ics_list",
    CREATE_IC_PRODUCT: "admin/ic/create_ic_product",
    GET_ALL_IC_PRODUCT: "admin/ic/get_all_ic_product",
    ACTIVATE_IC_PRODUCT: "admin/ic/activate_ic_product",
    DEACTIVATE_IC_PRODUCT: "admin/ic/deactivate_ic_product",
    ADD_IC_INTEREST_OPTION: "admin/ic/add_ic_interest_option",
    GET_IC_INTEREST_OPTIONS: "admin/ic/get_ic_interest_options",
    DELETE_IC_INTEREST_OPTIONS: "admin/ic/delete_ic_interest_options",
    UPDATE_IC_INTEREST_OPTIONS: "admin/ic/update_ic_interest_options",
};
export const I18N_PATH_CONSTANT = {
};