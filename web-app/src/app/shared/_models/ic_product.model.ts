export class IC_Product {
    name: string;
    interest: string;
    denomination: string;
    period: string;
    status: string;
}