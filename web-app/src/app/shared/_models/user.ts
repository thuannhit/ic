﻿export class TradingUser {
    id: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    token: string;
}
export class User {
    username: string;
    role: number;
    token: string;
}