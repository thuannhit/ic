import { resolve } from "dns";
import { rejects } from "assert";

const jQuery = require('jquery');
export class AJAX {
    call(oRequest, fnSuccess, fnError, bAsync) {
        let that=this;
        return new Promise((resolve, reject)=>{
            this.send(oRequest, bAsync).then(function (oRes) {
                if (oRes && oRes.results) {
                    resolve(oRes.results);
                    fnSuccess(oRes.results);
                }
            }, function (oError) {
                if (oError.xhr) {
                    reject(that._errorParser(oError.xhr.responseText))
                    fnError(that._errorParser(oError.xhr.responseText));
                }
            });
            
        }); 
    }
    // call(oRequest, fnSuccess, fnError, bAsync) {
    //     debugger;
    //     let that=this;
    //     return this.send(oRequest, bAsync).then(function (oRes) {
    //         if (oRes && oRes.results) {
    //             var oResult = oRes.results;
    //             if (oResult.message && oResult.message.defaultText) {
    //                 return fnError(oResult);
    //             }
    //             return fnSuccess(oResult);
    //         }
    //     }, function (oError) {
    //         if (oError.xhr) {
    //             return fnError(that._errorParser(oError.xhr.responseText));
    //         }
    //     });
    // }

    _errorParser(oErrorRes){
        if (typeof oErrorRes === "string") {
            try {
                var error = JSON.parse(oErrorRes);
                if (error) {
                    return error;
                }
            } catch (ex) {
                return oErrorRes;
            }
        }
        return oErrorRes;
    }

    send(oRequest, bAsync){
        var oDeferred = new jQuery.Deferred();
        var sData, aResults;
        if (!oRequest || !oRequest.url) {
            oDeferred.reject({
                error: "Property URL is not set. Cannot do ajax call"
            });
        }
        if (typeof oRequest.data === "object") {
            sData = JSON.stringify(oRequest.data);
        }
        jQuery.ajax({
            async: bAsync || false,
            data: sData,
            crossDomain: true,
            contentType: 'text/plain',
            xhrFields: {
                // The 'xhrFields' property sets additional fields on the XMLHttpRequest.
                // This can be used to set the 'withCredentials' property.
                // Set the value to 'true' if you'd like to pass cookies to the server.
                // If this is enabled, your server must respond with the header
                // 'Access-Control-Allow-Credentials: true'.
                withCredentials: false
            },

            type: oRequest.type || "GET",
            url: config.apiUrl+ oRequest.url,
            headers: oRequest.headers,
            error: (xhr, status, error) =>{
                oRequest.xhr = xhr;
                oRequest.status = status;
                oRequest.error = error;
                if (xhr && !oRequest.xhr.responseText && status === "timeout") {
                    oRequest.xhr.responseText = {
                        message: {
                            key: "ERROR_MSG_REQUEST_TIMEOUT"
                        }
                    };
                }
                oDeferred.reject(oRequest);
            },
            success: (data2, status, xhr) =>{
                oRequest.xhr = xhr;
                oRequest.status = status;
                oRequest.data = data2;
                aResults = data2;
                if (typeof data2 === "string") {
                    try {
                        aResults = JSON.parse(data2);
                    } catch (ex) {
                        // Not JSON oData - just return the string
                    }
                }
                if (aResults.d) {
                    aResults = aResults.d;
                    if (aResults.results) {
                        aResults = aResults.results;
                    }
                } else if (aResults.results) {
                    aResults = aResults.results;
                } else if (aResults.result) {
                    aResults = aResults.result;
                }
                oRequest.results = aResults;
                oDeferred.resolve(oRequest);
            }
        });
        return oDeferred.promise();
    }
}