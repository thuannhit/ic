import { AJAX } from '@/shared/_helpers';
export class DataProcessor {
    private ajax = new AJAX();
    constructor() { }
    private baseURI = "/ic/api/";
    UserLoginProcessing(inputData, fnSuccess, fnError, bAsync) {
        let serviceUrl = this.baseURI + inputData.url;
        let oRequest = {
            url: serviceUrl,
            type: "POST",
            data: inputData.data,
            headers: {
                "content-type": "application/json",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "X-Requested-With"
            }
        };
        return this.ajax.call(oRequest, fnSuccess, fnError, bAsync);
    }
    ICCreationProcessing(inputData, fnSuccess, fnError, bAsync) {
        let serviceUrl = this.baseURI + inputData.url;
        let oRequest = {
            url: serviceUrl,
            type: "POST",
            data: inputData.data,
            headers: {
                "content-type": "application/json",
                "Authorization": inputData.token,
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "X-Requested-With"
            }
        };
        return this.ajax.call(oRequest, fnSuccess, fnError, bAsync);
    }
    getICProductList(inputData, fnSuccess, fnError, bAsync) {
        let serviceUrl = this.baseURI + inputData.url;
        let oRequest = {
            url: serviceUrl,
            type: "GET",
            data: inputData.data,
            headers: {
                "content-type": "application/json",
                "Authorization": inputData.token,
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "X-Requested-With"
            }
        };
        return this.ajax.call(oRequest, fnSuccess, fnError, bAsync);
    }
    activateICProduct(inputData, fnSuccess, fnError, bAsync) {
        let serviceUrl = this.baseURI + inputData.url;
        let oRequest = {
            url: serviceUrl,
            type: "POST",
            data: inputData.data,
            headers: {
                "content-type": "application/json",
                "Authorization": inputData.token,
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "X-Requested-With"
            }
        };
        return this.ajax.call(oRequest, fnSuccess, fnError, bAsync);
    }

    processingWithGETMethod(inputData, fnSuccess, fnError, bAsync){
        let serviceUrl = this.baseURI + inputData.url;
        let oRequest = {
            url: serviceUrl,
            type: "GET",
            data: inputData.data,
            headers: {
                "content-type": "application/json",
                "Authorization": inputData.token,
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "X-Requested-With"
            }
        };
        return this.ajax.call(oRequest, fnSuccess, fnError, bAsync);
    }
    processingWithPOSTMethod(inputData, fnSuccess, fnError, bAsync){
        let serviceUrl = this.baseURI + inputData.url;
        let oRequest = {
            url: serviceUrl,
            type: "POST",
            data: inputData.data,
            headers: {
                "content-type": "application/json",
                "Authorization": inputData.token,
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "X-Requested-With"
            }
        };
        return this.ajax.call(oRequest, fnSuccess, fnError, bAsync);
    }
};