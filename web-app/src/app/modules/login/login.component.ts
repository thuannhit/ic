﻿import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { UserService } from '@/shared/_services';
import { AlertService } from '@/shared/_services_old';
import { InternationalizationService } from '../../shared/_services';

@Component({ encapsulation: ViewEncapsulation.None,  templateUrl: 'login.component.html', styleUrls: ['login.component.scss']})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    translationsUrl = '/assets/i18n/login';
    en: any = {
        "whyjoinus": "You will have special access",
        "createaccount": "Create Account"
    };
    et: any = {
        "whyjoinus": "ምምዝጋብ ሓለፋ ተጠቃማይነት ኣለዎ",
        "createaccount": "ተመዝገብ"
    };
    lan: any = this.en;
    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private userService: UserService,
        private alertService: AlertService,
        private inznservice: InternationalizationService,
    ) {
        // redirect to home if already logged in
        if (this.userService.currentUserValue) {
            this.router.navigate(['/']);
        }

        // this.inznservice.language.subscribe((val) => {
        //     if (val == "en") {
        //         this.lan = translate.use(val, this.translationsUrl).then((data) => {
        //             console.log(translate.translateObject);
        //         });
        //     }
        //     else if (val == "vn") {
        //         this.lan = translate.use(val, this.translationsUrl);
        //     }
        // });
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;
        let oInputData = {
            username: this.f.username.value,
            password: this.f.password.value,
            url:'user/login '
        },
            bAsync = true;
        // this.userService.login(oInputData, data => {
        //     debugger;
        // }, error => {
        //     debugger;
        // }, bAsync);
        // .pipe(first())
        // .subscribe(
        //     data => {
        //         this.router.navigate([this.returnUrl]);
        //     },
        //     error => {
        //         this.alertService.error(error);
        //         this.loading = false;
        //     });
    }
}


@Component({ encapsulation: ViewEncapsulation.None, templateUrl: 'login.component.html', styleUrls: ['login.component.scss'] })
export class AdminLoginComponent implements OnInit {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    translationsUrl = '/assets/i18n/login';
    // lan: any = this.en;
    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private userService: UserService,
        private alertService: AlertService,
        private inznservice: InternationalizationService,
    ) {
        // redirect to home if already logged in
        if (this.userService.currentUserValue) {
            this.router.navigate(['ic/admin/ic-management']);
        }
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;
        let oData = {
            username: this.f.username.value,
            password: this.f.password.value,
        }
        this.userService.adminLogin(oData, user => {
            this.router.navigate(['ic/admin/ic-management']);
        }, error => {
            debugger;
        });
    }
}
