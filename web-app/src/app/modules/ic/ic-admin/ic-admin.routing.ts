import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from '@/modules/home';
import { AuthGuard } from '@/shared/_helpers';
import { ICCreationComponent } from '@/modules/ic/components/ic-creation';
import { ICManagementComponent } from '@/modules/ic/components/ic-management';
import { ICInterstManagementComponent } from '@/modules/ic/components/ic-interest-management';
import { ICTradingManagementComponent } from '@/modules/ic/components/ic-trading';

const routes: Routes = [
    // { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'ic/admin/ic-creation', component: ICCreationComponent },
    { path: 'ic/admin/ic-management', component: ICManagementComponent },
    { path: 'ic/admin/ic-interest-management', component: ICInterstManagementComponent },
    { path: 'ic/admin/ic-trading-management', component: ICTradingManagementComponent },
    // { path: 'register', component: RegisterComponent },

    // otherwise redirect to home
    // { path: '**', redirectTo: '' }
];

export const icAdminRoutingModule = RouterModule.forChild(routes);