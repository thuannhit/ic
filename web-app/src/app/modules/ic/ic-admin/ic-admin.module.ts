import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
// import { Observable } from "rxjs";
// used to create fake backend
import { fakeBackendProvider } from '@/shared/_helpers';
import { ICCreationComponent, ICCreationDialogComponent } from '@/modules/ic/components/ic-creation';
import { ICManagementComponent } from '@/modules/ic/components/ic-management';
import { ICInterstManagementComponent } from '@/modules/ic/components/ic-interest-management';
import { ICInterestCreationDialogComponent } from '@/modules/ic/components/ic-interest-management/ic-interest-dialog';
import { ICPublishingDialogComponent } from '@/modules/ic/components/ic-publishing-dialog';
import { ICTradingManagementComponent } from '@/modules/ic/components/ic-trading';
import { icAdminRoutingModule } from './ic-admin.routing';
import { JwtInterceptor, ErrorInterceptor } from '@/shared/_helpers';
import { ICAdminComponent } from './ic-admin.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { TranslateService } from '@ngx-translate/core';
import { InternationalizationService } from '@/shared/_services';
import {
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatGridListModule,
    MatCardModule,
    MatTooltipModule,
    MatIconModule,
    MatTableModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatProgressBarModule,
    MatProgressSpinnerModule
} from '@angular/material';
import { MatMenuModule } from '@angular/material/menu';
// import {
//     NbChatModule,
//     NbDatepickerModule,
//     NbDialogModule,
//     NbMenuModule,
//     NbSidebarModule,
//     NbToastrModule,
//     NbWindowModule,
//     NbSelectModule, NbCardModule
// } from '@nebular/theme';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
// import { metaReducers, ROOT_REDUCERS } from './reducers';
import { makeStateKey, StateKey, TransferState } from '@angular/platform-browser';

@NgModule({

    imports: [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatRippleModule,
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
        HttpClientModule,
        icAdminRoutingModule,
        MatSelectModule,
        BrowserAnimationsModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatRadioModule,
        FormsModule,
        TranslateModule.forRoot(),
        MatGridListModule,
        MatCardModule,
        MatTooltipModule,
        // NbChatModule,
        // NbDatepickerModule,
        // NbDialogModule,
        // NbMenuModule,
        // NbSidebarModule,
        // NbToastrModule,
        // NbWindowModule,
        // NbSelectModule,
        // NbCardModule,
        MatMenuModule,
        MatIconModule,
        MatTableModule,
        MatToolbarModule,
        MatCheckboxModule,
        MatProgressBarModule,
        MatProgressSpinnerModule
    ],
    exports: [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatRippleModule,
        ReactiveFormsModule,
        HttpClientModule,
        MatSelectModule,
        BrowserAnimationsModule,
        MatDatepickerModule,
        ICAdminComponent,
        ICCreationDialogComponent,
        ICInterstManagementComponent,
        ICInterestCreationDialogComponent,
        ICPublishingDialogComponent,
        ICTradingManagementComponent
        // Observable
    ],
    declarations: [
        ICAdminComponent,
        ICCreationComponent,

        ICManagementComponent,
        ICInterstManagementComponent,
        ICCreationDialogComponent,
        ICInterestCreationDialogComponent,
        ICPublishingDialogComponent,
        ICTradingManagementComponent
    ],
    entryComponents: [ICCreationDialogComponent, ICInterestCreationDialogComponent, ICPublishingDialogComponent],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        InternationalizationService,
        // provider used to create fake backend
        fakeBackendProvider
    ],
    bootstrap: [ICAdminComponent]
})
export class ICAdminModule { };