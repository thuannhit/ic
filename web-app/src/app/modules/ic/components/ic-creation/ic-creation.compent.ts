import { Component, OnInit, ViewEncapsulation, AfterViewInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AlertService } from '@/shared/_services_old';
import { UserService, ICProductService, InternationalizationService, ValidationService } from '@/shared/_services';
import {API_CONSTANT} from '@/shared/_common';
@Component({ encapsulation: ViewEncapsulation.None,  selector: 'ic-creation', templateUrl: 'ic-creation.component.html', styleUrls: ['ic-creation.component.scss'] })
export class ICCreationComponent implements OnInit {
    icCreationForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    translationsUrl = '/assets/i18n/ic-admin';
    interests: { name: string, code: string }[] = [
        { name: '1%', code: '1' }, 
        { name: '2%', code: '2' },
        { name: '3%', code: '3' },
        { name: '4%', code: '4' },
        { name: '5%', code: '5' },
        { name: '6%', code: '6' },
        { name: '7%', code: '7' },
        { name: '8%', code: '8' },
        { name: '9%', code: '9' },
        { name: '10%', code: '10' },
        { name: '11%', code: '11' },
        { name: '12%', code: '12' },
        { name: '13%', code: '13' },
        { name: '14%', code: '14' },
        { name: '15%', code: '15' },
        { name: '16%', code: '16' },
        { name: '17%', code: '17' },
        { name: '18%', code: '18' },
        { name: '19%', code: '19' },
        { name: '20%', code: '20' },
    ];
    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private userService: UserService,
        private icProductSerice: ICProductService,
        private alertService: AlertService,
        private inznservice: InternationalizationService,
    ) {
        // redirect to home if already logged in
        if (this.userService.currentUserValue) {
            this.router.navigate(['/']);
        }
    }

    ngOnInit() {
        this.icCreationForm = this.formBuilder.group({
            icName: ['', Validators.required],
            icInterest: ['', Validators.required],
            icPeriod: ['', Validators.required],
            icDenomination: ['', Validators.required],
        });

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    // convenience getter for easy access to form fields
    get f() { return this.icCreationForm.controls; }

    onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.icCreationForm.invalid) {
            return;
        }

        this.loading = true;
        let oInputData = {
            data: {
                ic_name: this.f.icName.value,
                ic_interest_rate: this.f.icInterest.value,
                ic_period: this.f.icPeriod.value,
                ic_demnomination: this.f.icDenomination.value,
            },
            url: API_CONSTANT.CREATE_IC_PRODUCT
        },
            bAsync = true;
        this.icProductSerice.createICProduct(oInputData, data => {
            debugger;
        }, error => {
            debugger;
        }, );
    }
}

export interface DialogData {
    // animal: string;
    // name: string;
}

export interface InterestList {
    period: number, id: number, interest_rate: number
}

@Component({ encapsulation: ViewEncapsulation.None, selector: 'ic-creation', templateUrl: 'ic-creation.component.html', styleUrls: ['ic-creation.component.scss'] })
export class ICCreationDialogComponent implements OnInit {
    icCreationForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    translationsUrl = '/assets/i18n/ic-admin';
    icInterests: InterestList[] = [];
    selectedPeriod:{}
    constructor(
        public dialogRef: MatDialogRef<ICCreationDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private userService: UserService,
        private icProductService: ICProductService,
        private alertService: AlertService,
        private inznservice: InternationalizationService,) { }

    onNoClick(): void {
        this.dialogRef.close();
    }

    ngAfterViewInit():void{
        this.getICInterestList();
    }
    ngOnInit() {
        this.icCreationForm = this.formBuilder.group({
            icName: ['', Validators.required],
            icInterest: ['', [Validators.required, ValidationService.decimalValidation]],
            icPeriod: ['', [Validators.required]],
            icDenomination: ['', [Validators.required, ValidationService.numberValidation]],
        });
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }
    get f() { return this.icCreationForm.controls; }
    onCloseCreationDialog(){
        this.dialogRef.close();
    }

    onSubmit(){
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.icCreationForm.invalid) {
            return;
        }

        this.loading = true;
        let oData = {
            icProductName: this.f.icName.value,
            icInterest: this.f.icInterest.value,
            icPeriod: this.f.icPeriod.value.period,
            icDenomination: this.f.icDenomination.value,
        }
        this.icProductService.createICProduct(oData, oResult => {
            this.onCloseCreationDialog();
            // this.router.navigate(['ic/admin/ic-management']);
        }, oError => {
            console.log(oError);
        });
    }

    getICInterestList(): void {
        this.icProductService.getICInterestList({}, oResult => {
            this.icInterests = oResult.map(element => {
                return {
                    id: element.id, period: element.period, interest_rate: element.interest_rate
                }
            });
        }, oError => {
            console.log(oError);
        });
    }

}