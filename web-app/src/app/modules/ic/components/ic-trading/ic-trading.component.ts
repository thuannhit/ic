import { Component, OnInit, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { first } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { AlertService } from '@/shared/_services_old';
import { UserService, ICProductService, InternationalizationService, ValidationService } from '@/shared/_services';

import { ICCreationDialogComponent } from '@/modules/ic/components/ic-creation';
import { ICPublishingDialogComponent } from '@/modules/ic/components/ic-publishing-dialog';

export interface PublishedIC {
    ic_published_id: number;
    ic_product_id: number;
    ic_product_name: string;
    ic_published_base_price: number;
    ic_published_interest: number;
    ic_published_period: number;
    ic_available_published_amount: number;
    ic_published_current_price: number;
}
@Component({ encapsulation: ViewEncapsulation.None, templateUrl: 'ic-trading.component.html', styleUrls: ['ic-trading.component.scss'] })
export class ICTradingManagementComponent implements OnInit, AfterViewInit {
    icTradingForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    translationsUrl = '/assets/i18n/ic-trading';
    currentPublishedICs: PublishedIC[];
    tradingActions = [{ code: 'BUY', value: 'Buy' }, { code: 'SELL', value: 'Sell' }];
    currentFinanceInfo = {
        cashInAccount: 0,
        validCashForBuying: 0
    };
    selectedPublishedICForAction = {

    }
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userService: UserService,
        private alertService: AlertService,
        private inznservice: InternationalizationService,
        // private themeService: NbThemeService,
        public dialog: MatDialog,
        private formBuilder: FormBuilder,
        private icProductService: ICProductService,
    ) {
        // redirect to home if already logged in
        if (!this.userService.currentUserValue) {
            this.router.navigate(['/ic/admin/login']);
        }
    }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
        this.icTradingForm = this.formBuilder.group({
            tradingAction: ['', Validators.required],
            // icPeriod: ['', Validators.required],
            // icDenomination: ['', Validators.required],
            // publishedInterest: ['', Validators.required],
            actionAmount: ['', [Validators.required, ValidationService.numberValidation]],
            priceAction: ['', Validators.required],
            icCode: ['', Validators.required],
            totalCost: ['', [Validators.required, (control: AbstractControl) => {
                if (parseInt(control.value) && !!this.currentFinanceInfo && !!this.currentFinanceInfo.validCashForBuying) {
                    if (parseInt(control.value) < this.currentFinanceInfo.validCashForBuying){
                        return null
                    }
                }
                return { 'validCashAllowed': true };
            }]],
        });
    }
    get f() { return this.icTradingForm.controls; }
    ngAfterViewInit() {
        this.getPublishedICsList();
    }
    ngOnDestroy() { }

    onChangeTradingAction(): void {
        this.currentFinanceInfo = {
            cashInAccount: 1000000,
            validCashForBuying: 90000000000
        }
    }

    getPublishedICsList(): void {
        this.icProductService.getPublishedICsList({}, oResult => {
            this.currentPublishedICs = oResult.map(element => {
                return {
                    ic_published_id: element.id,
                    ic_product_id: element.ic_product,
                    ic_product_name: "NO NAME",
                    ic_published_base_price: element.ic_denomination,
                    ic_published_interest: element.published_interest,
                    ic_published_period: element.ic_period,
                    ic_available_published_amount: element.published_amount,
                    ic_published_current_price: element.ic_published_today_price,
                    ic_published_duedate: element.ic_published_duedate,
                    percent: Math.round(((new Date().getTime() - new Date(element.published_date).getTime()) / (element.ic_published_duedate - new Date(element.published_date).getTime())) * 100)
                }
            });
        }, oError => {
            console.log(oError);
        });
    }

    onSubmitTradingAction(): void {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.icTradingForm.invalid) {
            return;
        }
        debugger;
    }

}
