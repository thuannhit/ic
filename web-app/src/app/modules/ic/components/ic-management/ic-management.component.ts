import { Component, OnInit, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
// import { NbThemeService } from '@nebular/theme';
import { MatDialog } from '@angular/material/dialog';
import { AlertService } from '@/shared/_services_old';
import { UserService, ICProductService, InternationalizationService } from '@/shared/_services';

import { ICCreationDialogComponent } from '@/modules/ic/components/ic-creation';
import { ICPublishingDialogComponent } from '@/modules/ic/components/ic-publishing-dialog';
interface IC {
    icName: string;
    icInterest: string;
    icPeriod: string;
    icDenomination: string;
    icPublishedDate: string;
    icStatus: string;
    icPublishedICs: number;
}
@Component({ encapsulation: ViewEncapsulation.None, templateUrl: 'ic-management.component.html', styleUrls: ['ic-management.component.scss'] })
export class ICManagementComponent implements OnInit, AfterViewInit {
    icCreationForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    translationsUrl = '/assets/i18n/ic-management';
    // themeSubscription: any;
    // currentTheme: string;
    currentICs: IC[];

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userService: UserService,
        private alertService: AlertService,
        private inznservice: InternationalizationService,
        // private themeService: NbThemeService,
        public dialog: MatDialog,
        private icProductService: ICProductService,
    ) {
        // redirect to home if already logged in
        if (!this.userService.currentUserValue) {
            this.router.navigate(['/ic/admin/login']);
        }

        // this.themeSubscription = this.themeService.getJsTheme().subscribe(theme => {
        //     this.currentTheme = theme.name;
        // });
    }

    ngOnInit() {

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
        // this.initMockData();
    }

    initMockData() {
        this.currentICs = [{
            icName: "IC-TEST-01",
            icInterest: "1",
            icPeriod: "2",
            icDenomination: "1.000.000",
            icPublishedDate: "01 Dec, 2019",
            icStatus: "Active",
            icPublishedICs: 2500

        },
        {
            icName: "IC-TEST-02",
            icInterest: "2",
            icPeriod: "3",
            icDenomination: "3.000.000",
            icPublishedDate: "02 Dec, 2019",
            icStatus: "Active",
            icPublishedICs: 100
        }]
    }
    ngAfterViewInit() {
        this.getICProductList();
    }
    ngOnDestroy() {
        // this.themeSubscription.unsubscribe();
    }


    onOpenICCreationDialog(): void {
        const icCreationDialogRef = this.dialog.open(ICCreationDialogComponent, {
            width: '70%',
            // data: { name: this.name, animal: this.animal }
        });

        icCreationDialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }

    getICProductList(): void {
        this.icProductService.getICProductList({}, oResult => {
            this.currentICs = oResult;
        }, oError => {
            console.log(oError);
        });
    }

    onActivateICProduct(id): void {
        if (!!id && parseInt(id)) {
            this.icProductService.activateICProducts({ id: [id] }, oResult => {
                this.getICProductList();
            }, oError => {
                console.log(oError)
            })
        }
    }
    onDeactivateICProduct(id): void {
        if (!!id && parseInt(id)) {
            this.icProductService.deactivateICProducts({ id: [id] }, oResult => {
                this.getICProductList();
            }, oError => {
                console.log(oError)
            })
        }
    }
    onOpenICPublishingDialog(IC): void {
        if (!!IC && !!IC.id && parseInt(IC.id)) {
            const icPublishingDialogRef = this.dialog.open(ICPublishingDialogComponent, {
                width: '70%',
                data: {
                    id: IC.id,
                    product_name: IC.product_name,
                    period: IC.period,
                    denomination: IC.denomination,
                    status: IC.status,
                    interest: IC.interest_rate
                }
            });

            icPublishingDialogRef.afterClosed().subscribe(result => {
                console.log('The dialog was closed');
            });
        }
    }
}
