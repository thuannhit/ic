import { Component, OnInit, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
// import { NbThemeService } from '@nebular/theme';
import { MatDialog } from '@angular/material/dialog';
import { AlertService } from '@/shared/_services_old';
import { UserService, ICProductService, InternationalizationService } from '@/shared/_services';
import { MatTableDataSource } from '@angular/material';

import { ICInterestCreationDialogComponent } from '@/modules/ic/components/ic-interest-management/ic-interest-dialog';

export interface ICInterestRow {
    checked: boolean,
    id: number;
    period: number;
    interest: number;
    hovered?: boolean;
}

let ELEMENT_DATA: ICInterestRow[] = [];
@Component({ encapsulation: ViewEncapsulation.None, templateUrl: 'ic-interest-management.component.html', styleUrls: ['ic-interest-management.component.scss'] })
export class ICInterstManagementComponent implements OnInit, AfterViewInit {
    displayedColumns = ['checked', 'id', 'period', 'interest'];
    tableInterestData = new MatTableDataSource(ELEMENT_DATA);
    isSomeRowBeingChecked = false;
    applyFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
        this.tableInterestData.filter = filterValue;
    }
    icCreationForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    translationsUrl = '/assets/i18n/ic-management';

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userService: UserService,
        private inznservice: InternationalizationService,
        public dialog: MatDialog,
        private icProductService: ICProductService,
    ) {
        // redirect to home if already logged in
        if (!this.userService.currentUserValue) {
            this.router.navigate(['/ic/admin/login']);
        }
    }

    ngOnInit() {
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    ngAfterViewInit() {
        this.getICInterestList();
    }
    // ngOnDestroy() {
    //     this.themeSubscription.unsubscribe();
    // }

    getICInterestList(): void {
        this.icProductService.getICInterestList({}, oResult => {
            ELEMENT_DATA = oResult.map(element => {
                return {
                    checked: false, id: element.id, period: element.period, interest: element.interest_rate
                }
            });
            this.tableInterestData = new MatTableDataSource(ELEMENT_DATA);
        }, oError => {
            console.log(oError);
        });
    }

    onOpenAddInterestDialog(): void {
        const dialogRef = this.dialog.open(ICInterestCreationDialogComponent, {
            width: '40%',
        });

        dialogRef.afterClosed().subscribe(result => {
            this.getICInterestList();
            console.log('The dialog was closed');
        });
    }

    isSomeRowChecking() {

    }

    onCheckingRow(oData): void {
        this.isSomeRowBeingChecked = !this.tableInterestData.data.every(element => element.checked == false);
    }

    onRemoveInterestOptions(): void {
        let aCheckedRows = this.tableInterestData.data.filter(element => element.checked == true);
        let oData = Object.assign([], aCheckedRows || []).map(element => element.id);
        this.icProductService.removeICInterestOptions(oData, oResult => {
            this.getICInterestList();
        }, oError => {
            console.log(oError);
        });
    }
    onUpdateInterestOptions(): void {
        let aCheckedRows = this.tableInterestData.data.filter(element => element.checked == true);
        let oData = Object.assign([], aCheckedRows || []).map(element => { return { id: element.id, period: element.period, interest:element.interest}});
        this.icProductService.updateICInterestOptions(oData, oResult => {
            debugger;
            this.getICInterestList();
        }, oError => {
            console.log(oError);
        });
    }
}
