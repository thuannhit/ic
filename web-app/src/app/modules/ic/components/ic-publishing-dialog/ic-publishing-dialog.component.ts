import { Component, OnInit, AfterViewInit, ViewEncapsulation, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ICProductService, InternationalizationService, ValidationService } from '@/shared/_services';
import { AlertService } from '@/shared/_services_old';

export interface DialogData {
    id: number;
    product_name: string;
    period: number,
    denomination: number,
    status: string,
    interest: number

}
@Component({ encapsulation: ViewEncapsulation.None, templateUrl: 'ic-publishing-dialog.component.html', styleUrls: ['ic-publishing-dialog.component.scss'] })
export class ICPublishingDialogComponent implements OnInit {
    icPublishingForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    translationsUrl = '/assets/i18n/login';
    startDate: Date;
    minPublishedDate: Date;
    maxPublishedDate: Date;
    invalidPublishedDate = (d: Date): boolean => {
        const day = d.getDay();
        // Prevent Saturday and Sunday from being selected.
        return day !== 0 && day !== 6;
    };
    constructor(
        public dialogRef: MatDialogRef<ICPublishingDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private icProductService: ICProductService,
        private alertService: AlertService,
        private inznservice: InternationalizationService,
    ) {
    }

    ngOnInit() {
        this.icPublishingForm = this.formBuilder.group({
            icName: ['', Validators.required],
            icPeriod: ['', Validators.required],
            icDenomination: ['', Validators.required],
            publishedInterest: ['', Validators.required],
            publishedAmount: ['', [Validators.required, ValidationService.numberValidation]],
            publishedDate: ['', Validators.required],
        });

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
        const today = new Date();
        this.minPublishedDate = new Date(today);
        this.maxPublishedDate = new Date(today);
        this.minPublishedDate.setDate(this.minPublishedDate.getDate() + 1);
        this.maxPublishedDate.setDate(this.maxPublishedDate.getDate() + 30);
    }
    ngAfterViewInit() {

        // this.startDate=;
    }

    // convenience getter for easy access to form fields
    get f() { return this.icPublishingForm.controls; }

    onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.icPublishingForm.invalid) {
            return;
        }

        this.loading = true;
        let oInputData = {
            icProductId: this.data.id,
            icPeriod: this.f.icPeriod.value,
            icDenomination: this.f.icDenomination.value,
            publishedAmount: this.f.publishedAmount.value,
            publishedInterest: this.f.publishedInterest.value,
            publishedDate: this.f.publishedDate.value,
            url: '/ic/admin/publishedICs'
        };
        this.icProductService.publishIC(oInputData, oResult => {
            this.onCloseICPublishDialog();
        }, oError => {
            debugger;
        });
    }
    onCloseICPublishDialog() {
        this.dialogRef.close();
    }
}
