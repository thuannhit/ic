const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const router = express.Router();
import { RouterHandler} from './routes';
var cors = require('cors');
var whitelist = ['http://localhost:8080'];
var corsOptions = {
    origin: function (origin, callback) {
        if (whitelist.indexOf(origin) !== -1) {
            callback(null, true)
        } else {
            callback(new Error('Not allowed by CORS'))
        }
    }
}
// const routerHandler = new RouterHandler();
// This will be our application entry. We'll setup our server here.
const http = require('http');
// Set up the express app
const app = express();
// Log requests to the console.

app.use(logger('dev'));
// Parse incoming requests data (https://github.com/expressjs/body-parser)
app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.all('*', cors(corsOptions), function (req, res, next) {
    res.json({ msg: 'This is CORS-enabled for a whitelisted domain.' });
    next()
})
// app.all('*', function (req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "X-Requested-With");
//     res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
//     next()
// });
//
router.route("/ic/api")
    .all(RouterHandler);
// Setup a default catch-all route that sends back a welcome message in JSON format.
// app.get('*', (req, res) => res.status(200).send({
//     message: 'Welcome to the beginning of nothingness.',
// }));

const port = parseInt(process.env.PORT, 10) || 5000;
app.set('port', port);
const server = http.createServer(app);
console.log('Server is listening on port: '+port);
server.listen(port);
module.exports = app;