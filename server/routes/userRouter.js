
import express from 'express';
import bodyParser from 'body-parser';
import { UserController } from '../controllers';

let router = express.Router(),
    userController = new UserController();

router.use(bodyParser.json());
router.post('/user/login', userController.login);

export const UserRouter = router;