import { UserRouter } from './userRouter';

let express = require('express'),
    // controllers = require("../controllers"),
    // bodyParser = require('body-parser'),
    router = express.Router();

    router.use('/user', UserRouter)

export const RouterHandler = router;